#####Iniciar SpringBoot#######

mvn clean install

java -jar target/api

##### Configurar docker ####

mvn clean install
docker images
docker ps
docker build -t spring-boot-docker-demo .
docker run -p 8080:8080 -t api:latest

# Parar todos los contenedores
docker stop $(docker ps -a -q)

# Eliminar todos los contenedores
docker rm $(docker ps -a -q)

# Eliminar todas las im�genes
docker rmi $(docker images -q)

http://192.168.99.100:8090/hello-world

#Iniciar SpringBoot#

mvn spring-boot:run

####CRUD Spring Boot ####

http://www.bytestree.com/spring/restful-web-service-crud-operation-spring-boot-example/

### Rest Asured ###

https://semaphoreci.com/community/tutorials/testing-rest-endpoints-using-rest-assured

http://localhost:8090/employee/getAllEmployees

##Ejecutar LiquiBase##

mvn liquibase:update

## Configurar un microservicio ##

http://sinbugs.com/como-crear-un-microservicio-o-servicio-web-rest-con-spring-boot-1/
