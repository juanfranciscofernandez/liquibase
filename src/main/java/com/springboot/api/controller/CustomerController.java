package com.springboot.api.controller;

import com.springboot.api.model.Customer;
import com.springboot.api.service.CustomerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
/**
 * Created by Kiko2 on 15/09/2017.
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {
    final Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    CustomerService customerService;

    @RequestMapping(value = "/getAllCustomers", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<List<Customer>> getAllCustomers(Model model) {
        logger.info("---Method : getAllCustomers()---");
        List<Customer> customers = customerService.getAllCustomers();
        if (customers.isEmpty()) {
            logger.info("Employees does not exists");
            return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
        }
        logger.info("Found " + customers.size() + " Employees");
        logger.info(customers);
        logger.info(Arrays.toString(customers.toArray()));
        return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
    }

    @RequestMapping(value = "/getCustomer/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public Customer getCustomerById(@PathVariable int id) {
        logger.info("---Method : getCustomerById()---");
        return customerService.getCustomer(id);
    }

    @RequestMapping(value = "/addCustomer", method = RequestMethod.POST, headers = "Accept=application/json")
    public String addCustomer(@ModelAttribute("customer") Customer customer) {
        if(customer.getId()==0)
        {
            customerService.addCustomer(customer);
        }
        else
        {
            customerService.updateCustomer(customer);
        }

        return "redirect:/getAllCustomers";
    }

    @RequestMapping(value = "/updateCustomer/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public String updateCustomer(@PathVariable("id") int id,Model model) {
        model.addAttribute("customer", this.customerService.getCustomer(id));
        model.addAttribute("listOfCustomers", this.customerService.getAllCustomers());
        return "customerDetails";
    }

    @RequestMapping(value = "/deleteCustomer/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public String deleteCustomer(@PathVariable("id") int id) {
        customerService.deleteCustomer(id);
        return "redirect:/getAllCustomers";

    }
}

