package com.springboot.api.controller;

import com.springboot.api.model.Team;
import com.springboot.api.service.impl.TeamService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Kiko2 on 16/09/2017.
 */
@RestController
@RequestMapping("/team")
public class TeamController {

    final static Logger logger = Logger.getLogger(EmployeeController.class);

    @Autowired
    TeamService teamService;

    @RequestMapping(value="getAllTeams",method = RequestMethod.GET)
    public ResponseEntity<List<Team>> getAllEmployees() {
        List<Team> teams = teamService.getAll();
        if (teams.isEmpty()) {
            logger.debug("Employees does not exists");
            return new ResponseEntity<List<Team>>(HttpStatus.NO_CONTENT);
        }
        logger.debug("Found " + teams.size() + " Employees");
        logger.debug(teams);
        logger.debug(Arrays.toString(teams.toArray()));
        return new ResponseEntity<List<Team>>(teams, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    @CrossOrigin
    public ResponseEntity<Team> addEmployee(@RequestBody Team team) {
        teamService.save(team);
        logger.debug("Added:: " + team);
        return new ResponseEntity<Team>(team, HttpStatus.CREATED);
    }

}
