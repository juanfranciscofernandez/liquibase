package com.springboot.api.model;

import javax.persistence.*;

/**
 * Created by Kiko2 on 16/09/2017.
 */
@Entity
@Table(name="teams")
public class Team {

    @Id
    @Column(name="team_id")
    int team_id;

    @Column(name="logo")
    String logo;

    @Column(name="abbreviation")
    String abbreviation;

    @Column(name="team_name")
    String team_name;

    @Column(name="simple_name")
    String simple_name;

    @Column(name="location")
    String location;

    public Team() {
    }

    public Team(int team_id, String logo , String abbreviation, String team_name, String simple_name, String location) {
        this.team_id = team_id;
        this.logo = logo;
        this.abbreviation = abbreviation;
        this.team_name = team_name;
        this.simple_name = simple_name;
        this.location = location;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }


    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getSimple_name() {
        return simple_name;
    }

    public void setSimple_name(String simple_name) {
        this.simple_name = simple_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
