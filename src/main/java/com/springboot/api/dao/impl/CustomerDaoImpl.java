package com.springboot.api.dao.impl;

/**
 * Created by Kiko2 on 15/09/2017.
 */

import com.springboot.api.model.Customer;

import java.util.List;


public interface CustomerDaoImpl {
    public List<Customer> getAllCustomers() ;

    public Customer getCustomer(int id) ;

    public Customer addCustomer(Customer customer);

    public void updateCustomer(Customer customer) ;

    public void deleteCustomer(int id) ;
}
