package com.springboot.api.repository;

import com.springboot.api.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Kiko2 on 10/09/2017.
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
