package com.springboot.api.service;

import com.springboot.api.repository.EmployeeRepository;
import com.springboot.api.model.Employee;
import com.springboot.api.service.impl.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Kiko2 on 10/09/2017.
 */
@Service
public class DefaultEmployeeService implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee save(Employee entity) {
        return employeeRepository.save(entity);
    }

    @Override
    public Employee getById(Serializable id) {
            return employeeRepository.findOne((Long) id);
    }

    @Override
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @Override
    public void delete(Serializable id) {
        employeeRepository.delete((Long) id);
    }

}
