package com.springboot.api.service.impl;

import com.springboot.api.model.Team;

/**
 * Created by Kiko2 on 16/09/2017.
 */
public interface TeamService extends CRUDService<Team> {
}
