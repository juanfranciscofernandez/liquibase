package com.springboot.api.service.impl;

import com.springboot.api.model.Employee;

/**
 * Created by Kiko2 on 10/09/2017.
 */
public interface EmployeeService extends CRUDService<Employee> {
}

