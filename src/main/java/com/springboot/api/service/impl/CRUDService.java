package com.springboot.api.service.impl;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Kiko2 on 10/09/2017.
 */
public interface CRUDService<E> {

    E save(E entity);

    E getById(Serializable id);

    List<E> getAll();

    void delete(Serializable id);
}
