package com.springboot.api.service;

import com.springboot.api.model.Team;
import com.springboot.api.repository.TeamRepository;
import com.springboot.api.service.impl.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Kiko2 on 16/09/2017.
 */
@Service
public class DefaultTeamService implements TeamService {

    @Autowired
    private TeamRepository employeeRepository;

    @Override
    public Team save(Team entity) {
        return employeeRepository.save(entity);
    }

    @Override
    public Team getById(Serializable id) {
        return employeeRepository.findOne((Long) id);
    }

    @Override
    public List<Team> getAll() {
        return employeeRepository.findAll();
    }

    @Override
    public void delete(Serializable id) {
        employeeRepository.delete((Long) id);
    }


}
