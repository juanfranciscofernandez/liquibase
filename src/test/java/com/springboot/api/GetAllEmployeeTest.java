package com.springboot.api;
import com.jayway.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by Kiko2 on 10/09/2017.
 */
public class GetAllEmployeeTest {

    @BeforeClass
    public static void setBaseUri () {

        RestAssured.baseURI = "http://localhost:8090/employee/getAllEmployees";
    }

    @Test
    public void getAllEmployeeTest() {
        given()
                .when()
                .get("")
                .then()
                .statusCode(200);
    }
}
