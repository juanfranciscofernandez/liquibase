package com.springboot.api;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.springboot.api.model.Employee;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by Kiko2 on 11/09/2017.
 */
public class CreateEmployeeTest {
    @BeforeClass
    public static void setBaseUri () {

        RestAssured.baseURI = "http://localhost:8090/employee";
    }


    @Test
    public void sendPostObject () {

        Employee employee = new Employee();
        employee.setFirstname("Ataulfo");
        employee.setLastname("Gonzi");
        employee.setDesignation("123");
        employee.setSalary(12345);

        given()
                .body (employee)
                .when ()
                .contentType (ContentType.JSON)
                .post ("")
                .then()
                .statusCode(201);


    }
}
