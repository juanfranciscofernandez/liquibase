package com.springboot.api;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.springboot.api.model.Employee;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by Kiko2 on 11/09/2017.
 */
public class UpdateEmployeeTest {
    @BeforeClass
    public static void setBaseUri () {

        RestAssured.baseURI = "http://localhost:8090/employee/5";
    }


    @Test
    public void updateEmployee () {

        Employee employee = new Employee();
        employee.setId((long) 6);
        employee.setFirstname("Ataulfo");
        employee.setLastname("Pepe");
        employee.setDesignation("123");
        employee.setSalary(12345);

        given()
                .body (employee)
                .when ()
                .contentType (ContentType.JSON)
                .put ("")
                .then()
                .statusCode(200);


    }
}
