package com.springboot.api;

import com.jayway.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by Kiko2 on 11/09/2017.
 */
public class DeleteEmployee {

    @BeforeClass
    public static void setBaseUri () {

        RestAssured.baseURI = "http://localhost:8090/employee/6";
    }

    @Test
    public void deleteSingleEmployee() {
        given()
                .when()
                .delete("")
                .then()
                .statusCode(410);
    }
}
